#include<linux/module.h>
#include<linux/miscdevice.h>
#include<linux/init.h>
#include<linux/fs.h>
#include<linux/platform_device.h>
#include<linux/of.h>
#define DEVICE_NAME "my_device"
#define DUMMY0X99 0x01

/*platform data of dummy device */
struct dummy_platform_data
{
        int size;
        const char *serial_number;

};


const struct of_device_id dev_match_id = {

	compatible: "dummy_platfom_device", 
	data: (void *)DUMMY0X99,
};


int plat_probe(struct platform_device *pdev)
{

        pr_info("A device is detected\n");


// Exract the platform data

	struct dummy_platform_data detected_device;
	
	//Read serial number of device from device tree.
	of_property_read_string(pdev->dev.of_node,"org,serial-no", &detected_device.serial_number);

	//Read size property of device.
	of_property_read_u32(pdev->dev.of_node,"org,size",&detected_device.size);
	
	pr_info("Driver Probed");
	
	return 0;
}


int plat_remove(struct platform_device *pdev)
{

	pr_info("Driver Removed");
	return 0;
}


struct platform_driver plat_drv = {

	probe: plat_probe,
	remove: plat_remove,
	driver:{
		name: DEVICE_NAME,
  		of_match_table: &dev_match_id,	
	}	

};



static ssize_t device_write(struct file *file, const char __user *buff, size_t count, loff_t *ppos){

/*
                char kbuff =0;

                if(copy_from_user(&kbuff, buff, sizeof(kbuff)))
                        return -EFAULT ;

                pr_info("Recieved %d from user \n", kbuff);
*/
        return 1;
}




static ssize_t device_read(struct file *file, char __user *buff, size_t count , loff_t *ppos ){


/*        char kbuff = 'a' ;
        if(copy_to_user(buff, &kbuff, 1))

                pr_err("Failed to read\n");
//                return -EFAULT;

*/
        return 1;
}

static  struct file_operations plat_dev_ops = {

	read: &device_read,
        write: &device_write,
	owner: THIS_MODULE,
};

struct miscdevice mydev = {
	
	minor:MISC_DYNAMIC_MINOR ,
	name: DEVICE_NAME,
  	fops: &plat_dev_ops,
};




int __init plat_init(void){


	//Register platform device
	int ret = platform_driver_register(&plat_drv);
	if (ret < 0) {
	  pr_err("Registering PLatform device failed!");
	  return ret;
	}

	ret = misc_register(&mydev);

	if (ret < 0) {
	  pr_err("Registering char device failed!");
	  return ret;
	}


	pr_err("\nRegistered!");

	return 0;

}



void __exit plat_cleanup(void)
{
	platform_driver_unregister(&plat_drv);

}


module_init(plat_init);
module_exit(plat_cleanup);

MODULE_LICENSE("GPL");


