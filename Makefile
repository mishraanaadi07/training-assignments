ARCH ?= x86_64
CROSS_COMPILE ?= 
obj-m := my_platform_driver_dts.o 
INCLUDEDIR := /lib/modules/`uname -r`/build/
PWD := $(shell pwd)
default:
	make -C $(INCLUDEDIR) M=$(PWD) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) modules
clean:
	make -C $(INCLUDEDIR) M=$(PWD) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) clean

